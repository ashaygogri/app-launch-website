var gulp = require("gulp"),
    watch = require("gulp-watch"), //watcher
    autoprefixer = require("autoprefixer"),//sets the prefixes for css file
    postcss = require("gulp-postcss"),
    cssvars = require("postcss-simple-vars"),// used to define variables in css
    nested = require("postcss-nested"), //used to define nested css
    cssImport = require("postcss-import"),
    browserSync = require("browser-sync").create();//browserSync directly object nai banata isliye create method banaate h
    
gulp.task("css", function(){
   return gulp.src("./app/assets/styles/style.css")
         .pipe(postcss([cssImport,cssvars, nested, autoprefixer]))
         .pipe(gulp.dest("./app/css"));
});

gulp.task("watch", function(){
    browserSync.init({
        //json object
        notify: false,
        server:{
            baseDir: "app"
        }
    });
    watch("./app/index.html", function(){
        browserSync.reload();
    });
    watch("./app/assets/styles/**/*.css", gulp.series('css','cssInject')); // //**/* means styles ke andar jitna b folder hai usme jidar b .css hai
});

gulp.task("cssInject", function(){
   return gulp.src("./app/css/style.css")
    .pipe(browserSync.stream());
});
